<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Click;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;

/**
 * ClickRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ClickRepository extends EntityRepository
{
    /**
     * @param Click $click
     * @throws OptimisticLockException
     */
    public function insert(Click $click){
        $this->_em->persist($click);
        $this->_em->flush();
    }
}
