<?php

namespace AppBundle\DTOBuilder;

use AppBundle\Entity\Link;
use AppBundle\Entity\Stats;
use DateTime;

Class StatsDTOBuilder {

    protected $stats;
    protected $link;
    protected $dateTime;

    /**
     * StatsBuilder constructor.
     * @param Stats|null $stats
     */
    public function __construct($stats)
    {
        $this->stats = $stats;
    }

    /**
     * @param Link $link
     * @return $this
     */
    public function setLink(Link $link){
        $this->link = $link;
        return $this;
    }

    /**
     * @param DateTime $dateTime
     * @return $this
     */
    public function setDateTime(DateTime $dateTime){
        $this->dateTime = $dateTime;
        return $this;
    }

    /**
     * @return Stats
     */
    public function build()
    {
        if ($this->stats) {
            return $this->stats;
        }
        $this->stats = new Stats();
        $this->stats->setClicks(0);
        $this->stats->setImpressions(0);
        $this->stats->setLink($this->link);
        $this->stats->setDate($this->dateTime);
        $this->stats->setUpdatedTs($this->dateTime);
        return $this->stats;
    }
}