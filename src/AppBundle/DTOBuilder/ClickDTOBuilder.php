<?php

namespace AppBundle\DTOBuilder;

use AppBundle\Entity\Click;
use AppBundle\Entity\Link;
use DateTime;
use Symfony\Component\HttpFoundation\Request;

Class ClickDTOBuilder{

    protected $click;
    protected $link;

    /**
     * ClickDTOBuilder constructor.
     * @param Click $click
     */
    public function __construct(Click $click)
    {
        $this->click = $click;
    }

    /**
     * @param Link $link
     * @return $this
     */
    public function setLink(Link $link){
        $this->link = $link;
        return $this;
    }

    /**
     * @param Request $request
     * @return Click
     */
    public function build(Request $request)
    {
        $this->click->setCreatesTs(new DateTime());
        $this->click->setLink($this->link);
        $this->click->setRequest($request->query->all());
        $this->click->setIp($request->server->get('REMOTE_ADDR'));
        return $this->click;
    }
}