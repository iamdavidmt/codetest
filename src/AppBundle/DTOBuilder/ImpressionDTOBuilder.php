<?php

namespace AppBundle\DTOBuilder;

use AppBundle\Entity\Impression;
use AppBundle\Entity\Link;
use DateTime;
use Symfony\Component\HttpFoundation\Request;

class ImpressionDTOBuilder
{

    protected $impression;
    protected $link;

    /**
     * ImpressionDTOBuilder constructor.
     * @param Impression $impression
     */
    public function __construct(Impression $impression)
    {
        $this->impression = $impression;
    }

    /**
     * @param Link $link
     * @return $this
     */
    public function setLink(Link $link){
        $this->link = $link;
        return $this;
    }

    /**
     * @param Request $request
     * @return Impression
     */
    public function build(Request $request)
    {
        $this->impression->setLink($this->link);
        $this->impression->setIp($request->server->get('REMOTE_ADDR'));
        $this->impression->setReferringUrl('');
        if($request->server->get('HTTP_REFERER')) {
            $this->impression->setReferringUrl($request->server->get('HTTP_REFERER'));
        }
        $this->impression->setCreatedAt(new DateTime());
        return $this->impression;
    }
}
