<?php

namespace AppBundle\Controller;

use AppBundle\Service\ClickService;
use Doctrine\ORM\OptimisticLockException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class ClickController extends Controller
{
    protected $clickService;
    protected $logger;

    /**
     * ClickController constructor.
     * @param ClickService $clickService
     * @param LoggerInterface $logger
     */
    public function __construct(ClickService $clickService, LoggerInterface $logger)
    {
        $this->clickService = $clickService;
        $this->logger = $logger;
    }

    /**
     * @Route("/click", name="click")
     * @param Request $request
     * @return RedirectResponse
     * @throws InternalErrorException
     */
    public function addClick(Request $request)
    {
        try {
            $destination = $this->clickService->addClickAndGetDestination($request);
        } catch (OptimisticLockException $e) {
            throw new InternalErrorException($e->getMessage());
        }
        return $this->redirect($destination);
    }
}
