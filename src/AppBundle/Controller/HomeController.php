<?php

namespace AppBundle\Controller;

use AppBundle\Service\LinkService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends Controller
{

    protected $linkService;

    public function __construct(LinkService $linkService)
    {
        $this->linkService = $linkService;
    }

    /**
     * @Route("/", name="homepage")
     * @return Response|null
     */
    public function getHomepage()
    {
        $stats = $this->linkService->getLinkWithStats();
        return $this->render('default/index.html.twig', [
            'stats' => $stats
        ]);
    }
}
