<?php

namespace AppBundle\Controller;

use AppBundle\Service\LinkService;
use AppBundle\Validator\UrlValidator;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LinkController extends Controller
{

    protected $urlValidator;
    protected $linkService;

    public function __construct(UrlValidator $urlValidator, LinkService $linkService)
    {
        $this->urlValidator = $urlValidator;
        $this->linkService = $linkService;
    }

    /**
     * @Route("/link/add", name="link.add", methods={"POST"})
     * @param Request $request
     * @return RedirectResponse
     * @throws InternalErrorException
     */
    public function addLink(Request $request){
        try {
            $this->linkService->addLink($request);
            $this->addFlash('success', 'Link added successfully');
        } catch (OptimisticLockException $e){
            throw new InternalErrorException($e->getMessage());
        }
        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/link/edit/{id}", name="link.find", methods={"GET"})
     * @param $id
     * @return Response
     */
    public function getUpdateLinkForm($id){
        error_log('WRONG');
        error_log('WRONG');
        error_log('WRONG');
        error_log('WRONG');
        $link = $this->linkService->getLink($id);
        return $this->render('forms/link.html.twig', [
            'link'=>$link,
            'action'=> "/link/edit/$id",
        ]);
    }

    /**
     * @Route("/link/edit/{id}", name="link.update", methods={"POST"})
     * @param Request $request
     * @return Response
     * @throws InternalErrorException
     */
    public function updateLink(Request $request){
        error_log('OKKK');
        error_log('OKKK');
        error_log('OKKK');
        error_log('OKKK');
        try {
            $this->linkService->addLink($request);
            $this->addFlash('success', 'Link updated successfully');
        } catch (OptimisticLockException $e){
            throw new InternalErrorException($e->getMessage());
        }
        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/link/add", name="link.view", methods={"GET"})
     */
    public function getLinkForm()
    {
        return $this->render('forms/link.html.twig', [
            'action'=>'/link/add',
        ]);
    }
}
