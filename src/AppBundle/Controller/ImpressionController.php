<?php

namespace AppBundle\Controller;

use AppBundle\Service\ImpressionFileService;
use AppBundle\Service\ImpressionService;
use Doctrine\ORM\OptimisticLockException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;


class ImpressionController extends Controller
{
    protected $impressionService;
    protected $impressionFileService;
    protected $logger;

    /**
     * ImpressionController constructor.
     * @param ImpressionService $impressionService
     * @param ImpressionFileService $impressionFileService
     */
    public function __construct(ImpressionService $impressionService, ImpressionFileService $impressionFileService, LoggerInterface $logger)
    {
        $this->impressionService = $impressionService;
        $this->impressionFileService = $impressionFileService;
        $this->logger = $logger;
    }

    /**
     * @Route("/impression", name="impression", methods={"GET"})
     * @param Request $request
     * @return BinaryFileResponse
     * @throws InternalErrorException
     */
    public function getImpressionImage(Request $request)
    {
        try {
            $this->impressionService->addImpression($request);
        } catch (OptimisticLockException $e){
            throw new InternalErrorException($e->getMessage());
        }
        return new BinaryFileResponse($this->impressionFileService->getImpressionImage());
    }
}
