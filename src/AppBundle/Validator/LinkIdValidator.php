<?php

namespace AppBundle\Validator;

use Symfony\Component\HttpFoundation\Request;

class LinkIdValidator
{
    /**
     * @param Request $request
     * @return bool
     */
    public function validate(Request $request)
    {
        return $request->query->get('linkId') && $request->query->get('linkId') !== '';
    }
}
