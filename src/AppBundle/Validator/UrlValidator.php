<?php

namespace AppBundle\Validator;

use Symfony\Component\HttpFoundation\Request;

class UrlValidator
{
    /**
     * @param string $destination
     * @return bool
     */
    public function validate($destination)
    {
        return $this->destinationDefined($destination) && $this->isUrl($destination);
    }

    /**
     * @param $destination
     * @return bool
     */
    private function destinationDefined($destination){
        return $destination !== '';
    }

    /**
     * @param $destination
     * @return bool
     */
    private function isUrl($destination){
        return filter_var($destination, FILTER_VALIDATE_URL);
    }
}
