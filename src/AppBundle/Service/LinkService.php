<?php

namespace AppBundle\Service;

use AppBundle\Entity\Link;
use AppBundle\Entity\Stats;
use AppBundle\Repository\LinkRepository;
use Doctrine\ORM\OptimisticLockException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;

class LinkService
{
    protected $linkRepository;
    protected $logger;

    public function __construct(
        LoggerInterface $logger,
        LinkRepository $linkRepository
    )
    {
        $this->logger = $logger;
        $this->linkRepository = $linkRepository;
    }

    /**
     * @param $request
     * @throws OptimisticLockException
     */
    public function addLink(Request $request)
    {
        $link = (new Link())
            ->setName($request->request->get('name'))
            ->setDestination($request->request->get('destination'))
            ->setCreateTs(new \DateTime());

        $this->linkRepository->insert($link);
    }

    public function updateLink(Request $request){
        $link = $this->getLink($request->request->get('id'))
            ->setName($request->request->get('name'))
            ->setDestination($request->request->get('destination'))
            ->setCreateTs(new \DateTime());
        $this->linkRepository->insert($link);

    }

    /**
     * @param $id
     * @return object|null
     */
    public function getLink($id){
        return $this->linkRepository->find($id);
    }

    /**
     * @return array
     */
    public function getLinkWithStats(){
        return $this->linkRepository->getLinkWithStatsAndGroupByLink();
    }
}