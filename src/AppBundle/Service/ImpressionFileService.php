<?php

namespace AppBundle\Service;

use Psr\Log\LoggerInterface;
use Symfony\Component\Config\Resource\FileExistenceResource;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

class ImpressionFileService
{
    protected $logger;
    protected $file;

    /**
     * ImpressionFileService constructor.
     * @param String $file
     * @param LoggerInterface $logger
     */
    public function __construct($file, LoggerInterface $logger)
    {
        $this->file = $file;
        $this->logger = $logger;
    }

    /**
     * @return String
     */
    public function getImpressionImage()
    {
        if (!file_exists($this->file)) {
            throw new FileNotFoundException();
        }
        return $this->file;
    }
}