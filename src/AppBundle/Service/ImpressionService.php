<?php

namespace AppBundle\Service;

use AppBundle\DTOBuilder\ImpressionDTOBuilder;
use AppBundle\Entity\Impression;
use AppBundle\Entity\Link;
use AppBundle\Repository\ImpressionRepository;
use AppBundle\Repository\LinkRepository;
use Doctrine\ORM\OptimisticLockException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;

class ImpressionService
{
    protected $logger;
    protected $impressionRepository;
    protected $linkRepository;
    protected $statsService;

    public function __construct(
        LoggerInterface $logger,
        ImpressionRepository $impressionRepository,
        LinkRepository $linkRepository,
        StatsService $statsService
    )
    {
        $this->logger = $logger;
        $this->impressionRepository = $impressionRepository;
        $this->linkRepository = $linkRepository;
        $this->statsService = $statsService;
    }

    /**
     * @param Request $request
     * @throws OptimisticLockException
     */
    public function addImpression(Request $request)
    {
        $linkId = $request->query->get('linkId');

        /** @var Link $link */
        $link = $this->linkRepository->find($linkId);

        if($link === null){
            $this->logger->error("Link ID: $linkId doesnt exist for impression");
            return;
        }
        $impression = (new ImpressionDTOBuilder(new Impression()))->setLink($link)->build($request);
        $this->impressionRepository->insert($impression);
        $this->statsService->incrementImpression($link);
    }
}
