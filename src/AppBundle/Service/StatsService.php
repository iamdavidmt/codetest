<?php

namespace AppBundle\Service;

use AppBundle\DTOBuilder\StatsDTOBuilder;
use AppBundle\Entity\Link;
use AppBundle\Entity\Stats;
use AppBundle\Repository\StatsRepository;
use DateTime;
use Doctrine\ORM\OptimisticLockException;
use Psr\Log\LoggerInterface;

class StatsService
{
    protected $statsRepository;
    protected $logger;

    public function __construct(
        LoggerInterface $logger,
        StatsRepository $statsRepository
    )
    {
        $this->logger = $logger;
        $this->statsRepository = $statsRepository;
    }

    /**
     * @param Link $link
     * @throws OptimisticLockException
     */
    public function incrementClick(Link $link)
    {
        $stats = $this->prepareAndStatsBuild($link);
        $stats->setClicks($stats->getClicks() === 0 ? 1 : $stats->getClicks() + 1);
        $this->statsRepository->insertOrUpdate($stats);
    }

    /**
     * @param Link $link
     * @throws OptimisticLockException
     */
    public function incrementImpression(Link $link)
    {
        $stats = $this->prepareAndStatsBuild($link);
        $stats->setImpressions($stats->getImpressions() === 0 ? 1 : $stats->getImpressions() + 1);
        $this->statsRepository->insertOrUpdate($stats);
    }

    /**
     * @param Link $link
     * @return Stats|null
     */
    private function prepareAndStatsBuild(Link $link)
    {
        $dateTime = new DateTime();
        /** @var Stats $stats */
        $stats = $this->statsRepository->findByLinkAndDateTime($link, $dateTime);
        return (new StatsDTOBuilder($stats))
            ->setLink($link)
            ->setDateTime($dateTime)
            ->build();
    }
}
