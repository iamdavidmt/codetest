<?php

namespace AppBundle\Service;

use AppBundle\DTOBuilder\ClickDTOBuilder;
use AppBundle\Entity\Click;
use AppBundle\Entity\Link;
use AppBundle\Repository\ClickRepository;
use AppBundle\Repository\LinkRepository;
use Doctrine\ORM\OptimisticLockException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;

class ClickService
{
    protected $linkRepository;
    protected $statsService;
    protected $clickRepository;
    protected $logger;

    public function __construct(
        LoggerInterface $logger,
        LinkRepository $linkRepository,
        ClickRepository $clickRepository,
        StatsService $statsService
    )
    {
        $this->logger = $logger;
        $this->linkRepository = $linkRepository;
        $this->clickRepository = $clickRepository;
        $this->statsService = $statsService;
    }

    /**
     * @param Request $request
     * @return string
     * @throws OptimisticLockException
     */
    public function addClickAndGetDestination(Request $request){
        $linkId = $request->query->get('linkId');
        /** @var Link $link */
        $link = $this->linkRepository->find($linkId);
        if($link === null){
            $this->logger->error("Link ID: $linkId doesnt exist for click");
        }
        $click = (new ClickDTOBuilder(new Click()))->setLink($link)->build($request);
        $this->clickRepository->insert($click);
        $this->statsService->incrementClick($link);
        return $link->getDestination();
    }
}
