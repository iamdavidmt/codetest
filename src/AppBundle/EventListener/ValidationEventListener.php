<?php

namespace AppBundle\EventListener;

use AppBundle\Controller\ClickController;
use AppBundle\Controller\ImpressionController;
use AppBundle\Controller\LinkController;
use AppBundle\Validator\LinkIdValidator;
use AppBundle\Validator\UrlValidator;
use Psr\Log\LoggerInterface;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Symfony\Component\Routing\Router;

class ValidationEventListener
{
    /**
     * @var LinkIdValidator
     */
    protected $linkIdValidator;

    /**
     * @var UrlValidator
     */
    protected $urlValidator;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Router
     */
    protected $router;

    /**
     * ValidationEventListener constructor.
     * @param LinkIdValidator $linkIdValidator
     * @param UrlValidator $urlValidator
     * @param LoggerInterface $logger
     * @param Router $router
     */
    public function __construct(
        LinkIdValidator $linkIdValidator,
        UrlValidator $urlValidator,
        LoggerInterface $logger,
        Router $router
    )
    {
        $this->linkIdValidator = $linkIdValidator;
        $this->urlValidator = $urlValidator;
        $this->logger = $logger;
        $this->router = $router;
    }

    /**
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();
        if (!is_array($controller)) {
            return;
        }
        if ($controller[0] instanceof ImpressionController && $controller[0] instanceof ClickController) {
            if (!$this->linkIdValidator->validate($event->getRequest())) {
                throw new InvalidParameterException('Invalid Parameter: linkId');
            }
        }

        if($controller[0] instanceof LinkController){
            if($event->getRequest()->getMethod() === 'POST' && !$this->urlValidator->validate($event->getRequest()->get('destination'))){
                throw new InvalidParameterException('Invalid Parameter: destination');
            }
        }
    }

    public function onKernelException(GetResponseForExceptionEvent $event){
        $exception = $event->getException();
        if($exception instanceof FileNotFoundException){
            $this->logger->info($exception->getMessage());
        }
        if($exception instanceof FileNotFoundException){
            $this->logger->info($exception->getMessage());
        }
        if($exception instanceof InternalErrorException){
            $event->getRequest()->getSession()->getFlashBag()->add('error', 'There was an internal Error');
            $controllerName = 'homepage';
            if ($controllerName === $event->getRequest()->get('_route')) {
                return;
            }
            $response = new RedirectResponse($this->router->generate($controllerName));
            $event->setResponse($response);
        }
        if ($exception instanceof InvalidParameterException) {
            $event->getRequest()->getSession()->getFlashBag()->add('error', $exception->getMessage());
            $this->logger->info($exception->getMessage());
            $controllerName = 'link.view';
            if ($controllerName === $event->getRequest()->get('_route')) {
                return;
            }
            $response = new RedirectResponse($this->router->generate($controllerName));
            $event->setResponse($response);
        }
    }

}